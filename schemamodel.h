#ifndef SCHEMAMODEL_H
#define SCHEMAMODEL_H

#include <QString>
#include <QTreeWidgetItem>

class schemaModel : public QTreeWidgetItem
{
public:

inline void setName(QString _name){name = _name;}
inline void setType(QString _type){type = _type;}

inline QString getName(){return name;}
inline QString getType(){return type;}

private:

    QString name;
    QString type;

};

#endif // SCHEMAMODEL_H
