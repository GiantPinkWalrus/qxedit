#include "mainwindow.h"

MainWindow::MainWindow()
{
    QWidget *widget = new QWidget;
    setCentralWidget(widget);

    QWidget *topFiller = new QWidget;
    topFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QWidget *bottomFiller = new QWidget;
    bottomFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(5);
    layout->addWidget(topFiller);
    layout->addWidget(bottomFiller);
    widget->setLayout(layout);

    xmlTable = new XmlTableViewWidget;
    layout->addWidget(xmlTable);

    schemaName = new QLineEdit;
    schemaType = new QLineEdit;

    QLabel* schemaNameLabel = new QLabel("Element name:", this);

    QLabel* schemaNameType = new QLabel("Element type:", this);

    layout->addWidget(schemaNameLabel);
    layout->addWidget(schemaName);
    layout->addWidget(schemaNameType);
    layout->addWidget(schemaType);



    schemaWidget = new SchemaTreeWidget;
    schemaWidget->setColumnCount(2);
    schemaWidget->selectionMode();

    layout->addWidget(schemaWidget);

    createActions();
    createMenus();


    setWindowTitle(tr("QxEdit"));
    setMinimumSize(160, 160);
    resize(640, 480);
}


void MainWindow::newFile()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save XML Schema"), "",
                                                    tr("XML schema file(*.xsd)"));
    if (fileName != ""){
        QFile file(fileName);
        if (!file.open(QFile::WriteOnly | QFile::Text)) {
            QMessageBox::warning(this, tr("Warning"),
                                 tr("Cannot write file %1:\n%2.")
                                 .arg(fileName)
                                 .arg(file.errorString()));
            return;
        }
        QTextStream stream(&file);
                stream << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" << endl;
                stream << "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">"<<endl;
              /*  QTreeWidgetItemIterator *it(schemaWidget);

                while (*it)
                {

                    if ((*it)->checkState(1) == 2)
                    {
                        QVariant w;
                        w = (*it)->data(4, 0);
                        std::cout << "Selected item # " << w.toString().toStdString() << "\n";
                    }

                    ++it;

                }*/
    }

}

void MainWindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "",
                                                    tr("XML files(*.xml);; XML schema file(*.xsd)"));
    if (fileName != ""){
        QFile file(fileName);
        if(!file.open(QIODevice::ReadOnly)){
            QMessageBox::critical(this, tr("Error"), tr("Could not open file"));
            return;
        }
        QFileInfo info(fileName);
        info.suffix();
    }
}

void MainWindow::save()
{

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save XML"), "",
                                                    tr("XML files(*.xml)"));
    if (fileName != ""){
        QFile file(fileName);
        if (!file.open(QFile::WriteOnly | QFile::Text)) {
            QMessageBox::warning(this, tr("Warning"),
                                 tr("Cannot write file %1:\n%2.")
                                 .arg(fileName)
                                 .arg(file.errorString()));
            return;
        }
        QXmlStreamWriter writer(&file);
        writer.setAutoFormatting(true);
        writer.writeStartDocument("1.0");
        writer.writeStartElement(treeItems[0]->getName());

        for(int i = 0; i < xmlTable->rowCount(); i++){
            writer.writeStartElement(treeItems[1]->getName());
            for(int j = 0; j < xmlTable->columnCount(); j++){
                writer.writeTextElement(xmlTable->horizontalHeaderItem(j)->text(),
                                        xmlTable->item(i,j)->text());
            }
            writer.writeEndElement();
        }

        writer.writeEndElement();

        writer.writeEndDocument();
    }
}

void MainWindow::undo()
{

}

void MainWindow::redo()
{

}

void MainWindow::cut()
{

}

void MainWindow::copy()
{

}

void MainWindow::paste()
{

}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About Menu"),
            tr("This XML-editor is made with the tears of dead baby puppies"
               " and my crushed hopes and dreams"));
}

void MainWindow::aboutQt()
{

}

void MainWindow::newSchemaItem()
{
    schemaModel *item = new schemaModel();
    item->setName(schemaName->text());
    item->setType(schemaType->text());
    item->setText(0, item->getName());
    item->setText(1, item->getType());
    treeItems.append(item);
    if(schemaWidget->selectedItems().isEmpty()){
    schemaWidget->addTopLevelItem(item);
    }
    else{
    schemaWidget->selectedItems()[0]->addChild(item);
    }
    if(item->getType() != "element"){
        horzHeaders << item->getName();
       xmlTable->setColumnCount(xmlTable->columnCount()+1);
        QTableWidgetItem* qtwi = new QTableWidgetItem(item->getName(),QTableWidgetItem::Type);
    //    xmlTable->insertColumn(xmlTable->columnCount()+1);
        xmlTable->setHorizontalHeaderItem(xmlTable->columnCount(), qtwi);
        xmlTable->setHorizontalHeaderLabels(horzHeaders);
    }
}

void MainWindow::newRow()
{
    xmlTable->setRowCount(xmlTable->rowCount()+1);
}


void MainWindow::createActions(){
    newAct = new QAction(tr("Save XML Schema &n"), this);
    newAct->setShortcuts(QKeySequence::New);
    connect(newAct, SIGNAL(triggered()),this, SLOT(newFile()));

    openAct = new QAction(tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    connect(openAct, SIGNAL(triggered()),this, SLOT(open()));

    saveAct = new QAction(tr("&Save XML file"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    connect(saveAct, SIGNAL(triggered()),this, SLOT(save()));

    exitAct = new QAction(tr("E&xit"), this);
    connect(exitAct, SIGNAL(triggered()),this, SLOT(close()));

    undoAct = new QAction(tr("&Undo"), this);
    undoAct->setShortcuts(QKeySequence::Undo);
    connect(undoAct, SIGNAL(triggered()),this, SLOT(undo()));

    redoAct = new QAction(tr("&Redo"), this);
    redoAct->setShortcuts(QKeySequence::Redo);
    connect(redoAct, SIGNAL(triggered()),this, SLOT(redo()));

    cutAct = new QAction(tr("Cu&t"), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    connect(cutAct, SIGNAL(triggered()),this, SLOT(cut()));

    copyAct = new QAction(tr("&Copy"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    connect(copyAct, SIGNAL(triggered()),this, SLOT(copy()));

    pasteAct = new QAction(tr("&Paste"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    connect(pasteAct, SIGNAL(triggered()),this, SLOT(paste()));

    aboutAct = new QAction(tr("&About"), this);
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    aboutQtAct = new QAction(tr("About &QT"), this);
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    connect(aboutQtAct, SIGNAL(triggered()), this, SLOT(aboutQt()));

    newSchemaItemAct = new QAction(tr("New Schema Item"), this);
    connect(newSchemaItemAct, SIGNAL(triggered()), this, SLOT(newSchemaItem()));

    newRowAct = new QAction(tr("New Row"), this);
    connect(newRowAct, SIGNAL(triggered()), this, SLOT(newRow()));
}

void MainWindow::createMenus(){
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(saveAct);
    fileMenu->addAction(newAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(undoAct);
    editMenu->addAction(redoAct);
    editMenu->addAction(cutAct);
    editMenu->addAction(copyAct);
    editMenu->addAction(pasteAct);

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);
}

void MainWindow::contextMenuEvent(QContextMenuEvent *event){
    QMenu menu(this);
//    menu.addAction(cutAct);
//    menu.addAction(copyAct);
//    menu.addAction(pasteAct);
    menu.addAction(newRowAct);
    menu.addAction(newSchemaItemAct);
    menu.exec(event->globalPos());
}
