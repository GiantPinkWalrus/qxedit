#include <QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

//    QtQuick2ApplicationViewer viewer;
//    viewer.setMainQmlFile(QStringLiteral("qml/qXedit/main.qml"));
//    viewer.showExpanded();

    MainWindow window;
    window.show();

    return app.exec();
}
